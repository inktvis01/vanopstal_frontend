jQuery(function($){

	$(window).on('load',init);

    function init(){
        $('body').removeClass('no-js');

    }

});



$(document).ready(function() {

    /* homepage featured carousel */

  $('#myCarousel').carousel({
      interval:   5000
    });
    

    $('#myCarousel').on('slid.bs.carousel', function (e) {
        var id = $('.item.active').data('slide-number');

        $('.thumbs .thumb-item').removeClass('active');
        $('#carousel-selector-'+id).addClass('active');
    });

    $('#carousel-selector-0').addClass('active');
    

    /* mobile menu */
    
    
    $('.menu-toggle').click(function (e) {
        $('body').toggleClass('nav-open');
        e.preventDefault();
    });

    $('.account-toggle').click(function (e) {
        $('body').toggleClass('nav-open');
        /* hier nog de checkbox met id="#account" op checked zetten */
        e.preventDefault();
    });

    $('.filters-toggle').click(function (e) {
        $('body').toggleClass('filters-open');
        e.preventDefault();
    });


    /* top nav dropdowns */

    $('#top-nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).fadeIn(30);
      $(this).find('.dropdown-toggle').addClass("dropdown-active").delay(500);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).fadeOut(20);
      $(this).find('.dropdown-toggle').removeClass("dropdown-active");
    });

    /* main nav dropdowns */

    $('#main-nav li.dropdown').hover(function() {

        var dropdownList = $('.dropdown-menu');
        var dropdownOffset = $(this).offset();
        var offsetLeft = dropdownOffset.left;
        var dropdownWidth = dropdownList.width();
        var docWidth = $(window).width();

        var subDropdown = $('.dropdown-menu').eq(1);
        var subDropdownWidth = subDropdown.width();

        var isDropdownVisible = (offsetLeft + dropdownWidth <= docWidth);
        var isSubDropdownVisible = (offsetLeft + dropdownWidth + subDropdownWidth <= docWidth);


      $(this).find('.dropdown-menu').stop(true, true).delay(500).fadeIn(100);
        if (!isDropdownVisible || !isSubDropdownVisible) {
          $(this).find('.dropdown-menu').addClass("pull-right");
        } else {
          $(this).find('.dropdown-menu').removeClass("pull-right");
        }
      $(this).find('.dropdown-toggle').addClass("dropdown-active");

    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(500).fadeOut(100);
     $(this).find('.dropdown-menu').removeClass("pull-right");
      $(this).find('.dropdown-toggle').removeClass("dropdown-active").delay(500);
    });

    $('li.dropdown > a').click(function(){
      location.href = this.href;
    });


    /* Lightbox */
    $('.lightbox').featherlight();


    /* Stacktable */

    $('.responsive-stacktable').stacktable();




    /* homepage new products carousel */

        /*$('.carousel').slick({
          dots: true,
          infinite: true,
          speed: 500,
          fade: true,
          cssEase: 'linear'
        });*/





});





